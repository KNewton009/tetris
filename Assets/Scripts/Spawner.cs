﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject [] groups;

	// Use this for initialization
	void Start () {
	spawnNext();
	}

	public void spawnNext (){

		int randomNum = Random.Range(0, groups.Length);

		Instantiate(groups[randomNum], transform.position, Quaternion.identity);
	}
}
