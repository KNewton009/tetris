﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level : MonoBehaviour {

	public AudioSource levelSoundSource;
	public AudioClip levelSound;
	public int score;
	// Use this for initialization
	void Start () {
		levelSoundSource.PlayOneShot(levelSound);
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		GameObject.Find("Score Number").GetComponent<Text>().text = score.ToString();
	}
}
